package needle.vindows18.challenge.model;

import org.springframework.data.annotation.Id;

import java.net.URI;
import java.time.LocalDateTime;

@org.springframework.data.mongodb.core.mapping.Document(collection = "document")
public class Document {

    @Id
    String id;
    String name;
    LocalDateTime creationTime;
    Long size;
    URI path;
    User user;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public URI getPath() {
        return path;
    }

    public void setPath(URI path) {
        this.path = path;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
