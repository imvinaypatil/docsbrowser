package needle.vindows18.challenge.service;

import needle.vindows18.challenge.model.Document;
import needle.vindows18.challenge.model.User;
import needle.vindows18.challenge.repositories.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    DocumentRepository documentRepository;

    @Override
    public List<Document> findAll() {
        return documentRepository.findAll();
    }

    @Override
    public List<Document> findByName(String name) {
        return documentRepository.findByName(name);
    }

    @Override
    public List<Document> findByCreationTime(LocalDateTime time) {
        return documentRepository.findByCreationTime(time);
    }

    @Override
    public List<Document> findByUser(User user) {
        return documentRepository.findByUser(user);
    }

    @Override
    public void saveOrUpdateDocument(Document document) {
        documentRepository.save(document);
    }

    @Override
    public void deleteDocument(String id) {
        documentRepository.deleteById(id);
    }
}
