package needle.vindows18.challenge.service;

import needle.vindows18.challenge.model.Document;
import needle.vindows18.challenge.model.User;

import java.time.LocalDateTime;
import java.util.List;

public interface DocumentService {

    List<Document> findAll();
    List<Document> findByName(String name);
    List<Document> findByCreationTime(LocalDateTime time);
    List<Document> findByUser(User user);
    void saveOrUpdateDocument(Document document);
    void deleteDocument(String id);
}
