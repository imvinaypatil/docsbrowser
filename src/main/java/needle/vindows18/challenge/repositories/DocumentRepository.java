package needle.vindows18.challenge.repositories;

import needle.vindows18.challenge.model.Document;
import needle.vindows18.challenge.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface DocumentRepository extends MongoRepository<Document,String> {
    List<Document> findByName(String name);
    List<Document> findByCreationTime(LocalDateTime time);
    List<Document> findByUser(User user);
}
