package needle.vindows18.challenge.controller;

import needle.vindows18.challenge.model.Document;
import needle.vindows18.challenge.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/documents")
public class DocumentController {

    @Autowired
    DocumentService documentService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        List<Document> result = documentService.findAll();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/{name}")
    public ResponseEntity<?> getByName(@PathVariable("name")String name) {
        List<Document> result = new ArrayList<>();
        result = documentService.findByName(name);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> addOrUpdateDocument(@RequestBody Document document) {
        documentService.saveOrUpdateDocument(document);
        return new ResponseEntity<>("Document Added successfully",HttpStatus.OK);
    }

    @DeleteMapping
    public void deleteExpense(@RequestParam("id")String id) {
        documentService.deleteDocument(id);
    }

}
